Rails.application.routes.draw do
  #get '/', to: 'static_page#home'
  # post '/', to: 'static_page#create'
  get 'categories/:category_id/products', to:"products#index"
  get 'products/:product_id/like_products', to: "like_product#index"
  get 'comments/:comment_id/like_comments', to: "like_comments#index"
  get '/current_user', to: 'application#user_must_exist'
  post '/login', to:'session#login'
  post '/sign_up', to: 'register#sign_up'
  resources :clients, except: [:create]
  resources :like_comments
  resources :like_products
  resources :comments
  resources :categories
  
  resources :shippings
  resources :addresses
  resources :products
  post '/search', to:'products#search'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
