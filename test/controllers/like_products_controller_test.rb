require 'test_helper'

class LikeProductsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @like_product = like_products(:one)
  end

  test "should get index" do
    get like_products_url, as: :json
    assert_response :success
  end

  test "should create like_product" do
    assert_difference('LikeProduct.count') do
      post like_products_url, params: { like_product: { client_id: @like_product.client_id, product_id: @like_product.product_id } }, as: :json
    end

    assert_response 201
  end

  test "should show like_product" do
    get like_product_url(@like_product), as: :json
    assert_response :success
  end

  test "should update like_product" do
    patch like_product_url(@like_product), params: { like_product: { client_id: @like_product.client_id, product_id: @like_product.product_id } }, as: :json
    assert_response 200
  end

  test "should destroy like_product" do
    assert_difference('LikeProduct.count', -1) do
      delete like_product_url(@like_product), as: :json
    end

    assert_response 204
  end
end
