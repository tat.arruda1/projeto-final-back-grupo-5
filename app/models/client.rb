class Client < ApplicationRecord
    #before_update

    has_secure_password
    validates :password, length: { minimum: 4, maximum: 15}, if: :password
    validates :phone, :numericality => true, length: { minimum: 8, maximum: 8}, if: :phone
    validates :celphone,:numericality => true, length: { minimum: 9, maximum: 9}, if: :celphone
    validates_email_format_of :email
    validates :email, :uniqueness => true
    # validates_cpf_format_of :cpf
    validates_format_of :name, :with => /\A[^0-9`!@#\$%\^&*+_=]+\z/ #apenas letras no nome
    # validates :zip, correios_cep: true

    enum kind:{
        client: 0,
        admin: 1
    }

    #has_many :orders, :dependent => :destroy
    #has_many :products, :through => :orders
    #has_many :products
    has_one :address
end
