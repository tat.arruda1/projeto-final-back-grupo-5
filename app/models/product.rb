class Product < ApplicationRecord
    
    #mount_uploader :image, ImageUploader
    serialize :image, JSON #if you use SQLite, add this line
    #belongs_to :client

    
    has_many :cart_items
    belongs_to :category
    has_many :comments
    has_many :like_products
    
    #tabela de ligaçao entre orders e products
    has_many :placements
    has_many :orders, through: :placements


    validates :description, presence: true
    validates :dimension, :weight, :price, :numericality => true
    validates :name , :uniqueness => true, if: :name

    validate :non_zero

    def non_zero
        if self.quantity < 0
            self.errors.add(:quantity, "O campo não pode ser menor que zero")
        end
    end

    enum kind: {
        informatica: 0,
        alimento: 1,
        roupa: 2
    }
    
    # private

  #  def not_referenced_by_any_line_item
  #   unless line_items.empty?
  #     errors.add(:base, 'Line items present')
  #     throw :abort
  #   end
end
    

