class Address < ApplicationRecord
  belongs_to :client
  # validates :state, :numericality => false, length: { minimum: 2, maximum: 2}, if: :state
  # # validates :address_name, :zip, :street, :number, :neighborhood, :city, :state , presence: true
  validates :cep, correios_cep: true
  
end
