class Comment < ApplicationRecord
  belongs_to :product, counter_cache: true
  belongs_to :client
  has_many :like_comments
  #byebug

  validates :message, presence: true
end
