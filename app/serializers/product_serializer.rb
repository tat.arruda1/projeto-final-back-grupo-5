class ProductSerializer < ActiveModel::Serializer
  attributes :id, :name, :description, :price, :image, :quantity, :buy, :weight ,:dimension

  has_many :comments
end
