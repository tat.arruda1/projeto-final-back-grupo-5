class AddressSerializer < ActiveModel::Serializer
  attributes :id, :cep, :city, :state, :address_name, :street
  belongs_to :client
    
end
