class StaticPageController < ApplicationController
  def home
    @clients = Client.all

    render json: @clients
  end
  
  def create
    @client = Client.new(client_params)

    if @client.save
      render json: @client, status: :created, location: @client
    else
      render json: @client.errors, status: :unprocessable_entity
    end
  end
end
