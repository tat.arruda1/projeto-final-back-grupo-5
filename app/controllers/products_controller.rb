class ProductsController < ApplicationController
  before_action :set_product, only: [:show, :update, :destroy]

  def search
    #Client.find_by(email: params[:client][:email])
    @product = Product.find_by(name: params[:product][:name])
    byebug
    if @product.present?
      render json: @product
    end
  end


  # GET /products
  def index
    @products = if params[:category_id].present?
      Category.find(params[:category_id]).products
      @products = Product.all.order("created_at desc")
    else
      Product.all
    end

    render json: @products
  end

  # GET /products/1
  def show
    render json: @product
  end

  # POST /products
  def create
    @product = Product.new(product_params)

    if @product.save
      render json: @product, status: :created, location: @product
    else
      render json: @product.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /products/1
  def update
    if @product.update(product_params)
      render json: @product
    else
      render json: @product.errors, status: :unprocessable_entity
    end
  end

  # DELETE /products/1
  def destroy
    @product.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_product
      @product = Product.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def product_params

      params.require(:product).permit(:name, :description, :price, :weight, :dimension, :image, :comments_count, :quantity, :like_count, :category_id)
    end
end
