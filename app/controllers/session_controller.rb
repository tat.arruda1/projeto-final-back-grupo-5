class SessionController < ApplicationController
  def login
    
    #pega email do client_id
    @client = Client.find_by(email: params[:client][:email])
    #compara se essa password é a desse user_id
    @client = @client&.authenticate(params[:client][:password])
    #se a password for a mesma, client se torna o client_id
    if @client
      token = JsonWebToken.encode(client_id: @client.id)
      render json: {token: token, client: @client}

    #se nao for, a mostra unauthorized  
    else
      render json: {error: "unauthorized"}
    end

 
  end
end
