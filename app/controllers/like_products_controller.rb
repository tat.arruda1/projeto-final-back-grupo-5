class LikeProductsController < ApplicationController
  before_action :set_like_product, only: [:show, :update, :destroy]


  def index
    @like_products = if params[:product_id].present?
      Product.find(params[:product_id]).like_products
    else
      LikeProduct.all
    end

    render json: @like_products
  end

  # GET /like_products
  # def index
  #   @like_products = LikeProduct.all

  #   render json: @like_products
  # end

  # GET /like_products/1
  def show
    render json: @like_product
  end

  # POST /like_products
  def create
    @like_product = LikeProduct.new(like_product_params)

    if @like_product.save
      render json: @like_product, status: :created, location: @like_product
    else
      render json: @like_product.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /like_products/1
  def update
    if @like_product.update(like_product_params)
      render json: @like_product
    else
      render json: @like_product.errors, status: :unprocessable_entity
    end
  end

  # DELETE /like_products/1
  def destroy
    @like_product.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_like_product
      @like_product = LikeProduct.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def like_product_params
      params.require(:like_product).permit(:client_id, :product_id)
    end
end
