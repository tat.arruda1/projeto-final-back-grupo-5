class RegisterController < ApplicationController

  def sign_up
    @client = Client.new(client_params)
    
    
    if @client.save

      @address = Address.new(address_params.merge({:client_id => @client.id}))
      if @address.save
        render json: @client, status: :created
      end
    else
      render json: @client.errors, status: :unprocessable_entity
    end
  end

  private
    def client_params
      params.require(:client).permit(:name, :email, :password, :password_confirmation, :kind, :birthdate, :phone, :celphone, :cpf)
    end

    def address_params
      params.require(:address).permit(:street, :city, :state, :cpf, :cep, :address_name)
    end
end
