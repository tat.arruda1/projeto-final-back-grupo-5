class LikeCommentsController < ApplicationController
  before_action :set_like_comment, only: [:show, :update, :destroy]


  # def index
  #   @products = if params[:category_id].present?
  #     Category.find(params[:category_id]).products
  #     @products = Product.all.order("created_at desc")
  #   else
  #     Product.all
  #   end

  #   render json: @products
  # GET /like_comments
  def index
    @like_comment = if params[:comment_id].present?
      Comment.find(params[:comment_id]).like_comments
    else
      Product.all
    end

    render json: @like_comment
  end

  # GET /like_comments/1
  def show
    render json: @like_comment
  end

  # POST /like_comments
  def create
    @like_comment = LikeComment.new(like_comment_params)

    if @like_comment.save
      render json: @like_comment, status: :created, location: @like_comment
    else
      render json: @like_comment.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /like_comments/1
  def update
    if @like_comment.update(like_comment_params)
      render json: @like_comment
    else
      render json: @like_comment.errors, status: :unprocessable_entity
    end
  end

  # DELETE /like_comments/1
  def destroy
    @like_comment.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_like_comment
      @like_comment = LikeComment.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def like_comment_params
      params.require(:like_comment).permit(:client_id, :comment_id)
    end
end
