class ApplicationController < ActionController::API
     #se da acesso negado do cancan, execute essa funcao
    rescue_from CanCan::AccessDenied do |exception|
        render json: {message: "Permissao Negada1"}
    end


    #Criamos essas funcoes para que sejam acessiveis a todos os metodos
    def current_user
        #pega o header que você esta enviando
        header = request.headers["Authorization"]
        #retira espaço caso vier a acontecer no header
        header = header.split(' ').last if header
        #retorna se header estiver presente
        return nil unless header.present?
        #descriptografa o id a partir da chave enviado no token
        @decoded = JsonWebToken.decode(header)
        # byebug
        #se o @decoded for um valor valido vai enviar

        return nil unless @decoded.present?
        #vai procurar user_id
        client = Client.find_by(id: @decoded[0]["client_id"])
    end

    # current_user.kind json
    #se voce estiver logado vai aparacer a mensagem
    def user_must_exist
        #byebug
        if current_user.present?
            render json: {message: "Voce esta logado", client: current_user}
        else
            render json: {message: "deslogado"}, status: 404
        end
    end

    protected
    #impede que user mesmo nao estando logado possa fazer alteracao em outro user
        def must_be_sigining_in
            if current_user.nil?
                render json: {message: "Permissao negada2"}
            end
        end


end
