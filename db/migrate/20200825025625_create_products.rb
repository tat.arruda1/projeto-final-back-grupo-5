class CreateProducts < ActiveRecord::Migration[5.2]
  def change
    create_table :products do |t|
      t.string :name
      t.string :description
      t.decimal :price, precision: 5, scale: 2, default: 0
      t.float :weight
      t.float :dimension

      t.timestamps
    end
  end
end
