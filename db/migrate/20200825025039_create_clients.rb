class CreateClients < ActiveRecord::Migration[5.2]
  def change
    create_table :clients do |t|
      t.string :name
      t.string :email
      t.string :password_digest
      t.integer :kind, default: 0
      t.date :birthdate
      t.string :phone
      t.string :celphone

      t.timestamps
    end
  end
end
