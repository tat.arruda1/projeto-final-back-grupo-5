class AddBuyToProducts < ActiveRecord::Migration[5.2]
  def change
    add_column :products, :buy, :integer,  default: 0
  end
end
