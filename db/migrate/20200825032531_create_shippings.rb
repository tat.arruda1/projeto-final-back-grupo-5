class CreateShippings < ActiveRecord::Migration[5.2]
  def change
    create_table :shippings do |t|
      t.references :client, foreign_key: true
      t.references :product, foreign_key: true
      t.string :service
      t.references :address, foreign_key: true

      t.timestamps
    end
  end
end
