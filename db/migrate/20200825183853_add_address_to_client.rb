class AddAddressToClient < ActiveRecord::Migration[5.2]
  def change
    add_column :clients, :address_name, :string
    add_column :clients, :zip, :string
    add_column :clients, :street, :string
    add_column :clients, :number, :string
    add_column :clients, :neighborhood, :string
    add_column :clients, :city, :string
    add_column :clients, :state, :string
  end
end
