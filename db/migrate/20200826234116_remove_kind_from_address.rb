class RemoveKindFromAddress < ActiveRecord::Migration[5.2]
  def change
    remove_column :addresses, :kind, :string
  end
end
