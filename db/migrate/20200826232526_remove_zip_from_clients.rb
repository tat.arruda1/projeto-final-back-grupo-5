class RemoveZipFromClients < ActiveRecord::Migration[5.2]
  def change
    remove_column :clients, :zip, :string
    remove_column :clients, :street, :string
    remove_column :clients, :number, :string
    remove_column :clients, :neighborhood, :string
    remove_column :clients, :city, :string
    remove_column :clients, :state, :string
    remove_column :clients, :address_name, :string
  end
end
