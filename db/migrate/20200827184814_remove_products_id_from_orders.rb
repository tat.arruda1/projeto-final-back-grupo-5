class RemoveProductsIdFromOrders < ActiveRecord::Migration[5.2]
  def change
    remove_reference :orders, :product, index: true, foreign_key: true 
  end
end
