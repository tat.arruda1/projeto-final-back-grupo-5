class RemoveKindFromProduct < ActiveRecord::Migration[5.2]
  def change
    remove_column :products, :kind, :integer
  end
end
