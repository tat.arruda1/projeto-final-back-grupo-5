class AddLikeCounterToProducts < ActiveRecord::Migration[5.2]
  def change
    add_column :products, :like_count, :integer, default: 0
  end
end
