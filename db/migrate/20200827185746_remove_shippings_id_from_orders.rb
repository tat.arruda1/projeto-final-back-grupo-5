class RemoveShippingsIdFromOrders < ActiveRecord::Migration[5.2]
  def change
    remove_reference :orders, :shipping, index: true, foreign_key: true
  end
end
