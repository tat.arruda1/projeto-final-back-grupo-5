class AddCommentCounterToProduct < ActiveRecord::Migration[5.2]
  def change
    add_column :products, :comments_count, :integer, default: 0
  end
end
