class AddAddressNameToAddress < ActiveRecord::Migration[5.2]
  def change
    add_column :addresses, :address_name, :string
    add_column :addresses, :street, :string
  end
end
