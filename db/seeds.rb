# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
20.times do
    Client.create(name: Faker::BossaNova.artist, password: '123456', password_confirmation: '123456', email: Faker::Internet.email, phone: '12345678', celphone: "123456789", cpf: "10784424756")

    Address.create(cep: "26186627", city: "Rio de Janeiro", state: "RJ", address_name: "Casa 1", street: "Rua atlantica")
end
# Client.create(name: 'OLA')

# 20.times do
#     Product.create(name: Faker::Food.fruits , description: Faker::Food.description, price: '40', weight: '2', dimension: '200', kind: 'roupa')
# end
# # 20.times do 
# #     Product
# # end
# # Client.create(name: 'OLA')
# 10.times do
#     Product.create(name: Faker::Commerce.unique.product_name, description: Faker::Commerce.color, price: Faker::Commerce.price, weight: 2.5, dimension: 5.5, kind: 0)
# end
